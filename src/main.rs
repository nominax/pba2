use std::env;
use simplelog::*;
use std::fs::File;
use calamine::{Reader, Xlsx, open_workbook,DataType};


use std::io::{self, BufRead};
use std::path::Path;


use xlsxwriter::{Format, Workbook, Worksheet};
use xlsxwriter::prelude::*;


const FONT_ROW: f64 = 11.0;
//const FONT_ROT: f64 = 10.0;
const FONT_SIZE_H1: f64 = 11.5;
const FONT_SIZE_H2: f64 = 11.0;

 



fn main() {
    let args: Vec<String> = env::args().collect();

    let partidas_p = args[args.len()-1].as_str();
    let file_xlsx = args[args.len()-2].as_str();
    let rutafx = args[args.len()-3].as_str();
    let paralog = format!("{}/c_isrtp.log", rutafx);
    let ruta_parti = format!("{rutafx}/{partidas_p}");
    CombinedLogger::init(
        vec![
            TermLogger::new(LevelFilter::Warn, Config::default(), TerminalMode::Mixed, ColorChoice::Auto),
            WriteLogger::new(LevelFilter::Info, Config::default(), File::create(paralog).unwrap()),
        ]
    ).unwrap();
    info!("Init-log");
    let inic=file_xlsx.len()-13;
    let cfin=file_xlsx.len()-5;    
    let parte_nomb= file_xlsx[inic..cfin].to_string();
    let excel_name = format!("isrtp_{parte_nomb}.xlsx");
    let uuid = format!("{}/{}",rutafx,excel_name);
    let workbook = Workbook::new(&uuid).expect("Error_creando wbok");
   

        let mut sheet = workbook.add_worksheet(Some(&parte_nomb)).expect("Error_creandoshet");
        let mut excel: Xlsx<_> = open_workbook(file_xlsx).unwrap();
        let nombre = excel.sheet_names();

        if let Some(Ok(r)) = excel.worksheet_range(&nombre[0]) {

            let _ = sheet.merge_range(1,0,1,4, "SUPREMO TRIBUNAL DE JUSTICIA DEL ESTADO DE SONORA" , Some(Format::new().set_align(FormatAlignment::Center).set_font_size(FONT_SIZE_H1)));
            let _ = sheet.merge_range(2,0,2,4, "OFICIALIA MAYOR" , Some(Format::new().set_align(FormatAlignment::Center).set_font_size(FONT_SIZE_H1)));
            let _ = sheet.merge_range(3,0,3,4, "DIRECCIÓN GENERAL DE RECURSOS HUMANOS Y MATERIALES" , Some(Format::new().set_align(FormatAlignment::Center).set_font_size(FONT_SIZE_H1)));
            let _ = sheet.merge_range(5,0,5,4, &format!("BASE GRAVABLE - IMPUESTO ISRTP - {}",parte_nomb.to_string()) , Some(Format::new().set_align(FormatAlignment::Center).set_bold().set_font_size(FONT_SIZE_H2)));
            let _ = sheet.merge_range(6,0,6,4, "(Información proporcionada por la Dirección de Contabilidad y Control Presupuestal)" , Some(Format::new().set_align(FormatAlignment::Center).set_font_size(10.00)));

            for (col , valx ) in [(0,"Posicion Presupuestaria"),(1,"Descripcion"),(2,"Cantidad")] {
                let c0l= valx.to_string();
                create_rotulos(&mut sheet, (col as u16) + 1, &c0l);
            }
            let mut acmula_grav:f64 = 0.0;
            let base_grav:f64 = 0.03;
            let mut renglonx = 8;
            //let comparati= r.rows().len()-1;

            if let Ok(lines) = read_lines(ruta_parti) {
                for line in lines { 
                    if let Ok(parti) = line {
                        renglonx +=1;
                        if r.rows().filter(|rowest| rowest[0].to_string().trim() == parti.trim()).count() != 1 {panic!("La partida --> {} no se pagara o quiza ESTAN 2 PARTIDAS IGUALES EN ARCHIVO ORIGEN XLSX",parti) } 
                        match r.rows().filter(|rowest| rowest[0].to_string().trim() == parti.trim()).next() {
                            Some(encontro) => { 
                                for (col , valx ) in encontro.iter().enumerate(){
                                    let flotante = if col > 1 {                                   
                                        match valx {
                                            DataType::Float(f) => acmula_grav += f,
                                            DataType::Int(i) => acmula_grav += *i as f64,
                                            DataType::String(s) => {

                                                if let Ok(f) = s.parse::<f64>() {
                                                    acmula_grav += f;
                                                }else{panic!("Panico basegrav float x1");}

                                            },
                                            _ => panic!("en basegrav x2"),
                                        }
                                        "fl"
                                    }else {"str"};
                                    let c0l= valx.to_string(); 
                                    create_row_columnx(&mut sheet, renglonx, (col as u16) + 1 , &c0l ,flotante,"no");
                                }
                            },
                            _ => panic!("Una Partida Definida para Pago  NO ESTA EN ARCHIVO EXCEL NACHO!"), 
                        }
                         
                    }
                }
                let a2digitos= format!("{acmula_grav:.2}");
                let apagar = format!("{:.2}",a2digitos.parse::<f64>().unwrap() * base_grav);
                let row = renglonx + 1;
                create_row_columnx(&mut sheet, row , 3, &a2digitos ,"fl","ult");
                create_row_columnx(&mut sheet, row+1 , 3 ,&apagar ,"fl","ult");
                let _ = sheet.merge_range(row,1, row,
                    2, "Base Gravame - " , Some(Format::new().set_align(FormatAlignment::Right).set_bold().set_border(FormatBorder::Thin).set_font_size(FONT_SIZE_H2)));
            
                let _ = sheet.merge_range(row+1 ,1,row+1,
                    2, "Impuesto ISRTP - " , Some(Format::new().set_align(FormatAlignment::Right).set_bold().set_border(FormatBorder::Thin).set_font_size(FONT_SIZE_H2)));
            }

            let _ = sheet.insert_image_opt(0, 0, "logox.png",&ImageOptions{
                x_offset: 19,
                y_offset: 17,
                x_scale: 1.8,
                y_scale: 1.8,
            });
        
        
        }

    workbook.close().expect("workbook can be closed");
    info!("fin--> todo ok {parte_nomb}");



}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


fn create_row_columnx(sheet: &mut Worksheet,renglon:u32, columnx:u16, rotulx:&str, flotax:&str, ultima:&str) {
    let mut elformato_rot = Format::new();
    let rengx = renglon as u16;
    let rotulo_fmt = if ultima.contains("no") { elformato_rot.set_border(FormatBorder::Thin)
    .set_font_size(FONT_ROW)} else{
        if flotax.contains("fl") { elformato_rot.set_border(FormatBorder::Thin).set_bold().set_num_format("$ #,###,##0.00").set_font_size(FONT_SIZE_H2)} else {
            elformato_rot.set_border(FormatBorder::Thin).set_bold().set_font_size(FONT_SIZE_H2)
        }
    };

    if flotax.contains("fl") {
        let _ = sheet.write_number(renglon, columnx, rotulx.parse().unwrap() ,  Some(&rotulo_fmt));
    } else{
        let _ = sheet.write_string(renglon, columnx, rotulx, Some(&rotulo_fmt));
    }
    let distanciax = if columnx == 1 { 14.0 } else { if columnx == 2 { 22.0 } else{
        19.0 }
    };
    let _ = sheet.set_column(rengx, columnx , distanciax, None);
    let _ = sheet.set_column(rengx, 4 , 9.0, None);


}



fn create_rotulos(sheet: &mut Worksheet, columnx:u16, rotulx:&str,) {
    let mut elformato_rot = Format::new();
    let rotulo_fmt = elformato_rot.set_bg_color(FormatColor::Custom(0xDCEDC8)).set_bold()
    .set_text_wrap()
    .set_align(FormatAlignment::Center)
    .set_border(FormatBorder::Thin)
    .set_font_size(FONT_SIZE_H2);     

    let _ = sheet.write_string(8, columnx, rotulx, Some(&rotulo_fmt));
    //let _ = sheet.set_column(8, 1, 20.0, None);
    //set_new_max_width(columnx, longitudes , &mut width_map);   
}




